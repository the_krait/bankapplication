<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
function validate(){
	var name = document.registerform.person_name.value;
	var address = document.registerform.person_address.value;
	var uidai = document.registerform.person_uidai_number.value;
	var mobile_no = document.registerform.person_mobile_number.value;
	
	if(name==null || name=="") {
		alert("Name field should not be empty")
		return false;
	}
	else if(address==null || address=="") {
		alert("Address field should not be empty")
		return false;
	}
	else if(person_uidai_number<0 && person_uidai_number>12) {
		alert("UIDAI number must be 12 digits")
		return false;
	}
	else if(person_mobile_number<0 && person_mobile_number>10) {
		alert("Mobile number must be 10 digits")
		return false;
	}
	else {
		return true;
	}
}
</script>
<meta charset="ISO-8859-1">
<title>Registration</title>
</head>
<body>
	<form action="register" name="registerform" method="POST" onsubmit="return validate()">
		<table border="2" align="center" bgcolor="lightblue">
			<tr>
				<td>Name:</td>
				<td><input type="text" required="required" name="customerName"></td>
			</tr>
			<tr>
				<td>Address:</td>
				<td><input type="text" required="required" name="customerAddr"></td>
			</tr>
			<tr>
				<td>UIDAI Number:</td>
				<td><input type="number" maxlength="12" required="required" name="customerUIDAI"></td>
			</tr>
			<tr>
				<td>Mobile/Telephone:</td>
				<td><input type="number" maxlength="10" required="required" name="customerMobile"></td>
			</tr>
			<tr>
				<td>Email Address:</td>
				<td><input type="email" required="required" name="customerEmail"></td>
			</tr>
			<tr>
				<td>Amount Deposited:</td>
				<td><input type="number" required="required" maxlength="10" required="required" name="amountDepo"></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Register"></td>
			</tr>
		</table>
		<h2 color="red">${message}</h2>
	</form>
</body>
</html>