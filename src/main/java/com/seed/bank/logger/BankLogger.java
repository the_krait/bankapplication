package com.seed.bank.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.seed.bank.controller.RegistrationController;

public class BankLogger {

	private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

	public static Logger getLogger() {
		return logger;
	}

}
