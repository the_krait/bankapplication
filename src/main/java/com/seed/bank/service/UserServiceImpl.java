package com.seed.bank.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.seed.bank.model.User;
import com.seed.bank.repository.UserRepository;

@Component
public class UserServiceImpl implements UserService {

	private static final Logger userServicelogger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	UserRepository userRepository;
	
	User validUser;
	
	@Override
	public boolean validateUser(User user) {
		
		userServicelogger.info("In UserServiceImpl:validateUser() method");
		long customerId = user.getCustomerId();
		String custPassword = user.getCustomerPassword();
		validUser = userRepository.findByCustomerIdAndCustomerPassword(customerId,custPassword);
		if(validUser==null) {
			return false;
		}
		return true;
	}

	public User getValidUser() {
		userServicelogger.info("In UserServiceImpl:getValidUser() method");
		return validUser;
	}

	public void setValidUser(User validUser) {
		userServicelogger.info("In UserServiceImpl:setValidUser() method");
		this.validUser = validUser;
	}
}
