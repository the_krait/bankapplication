package com.seed.bank.service;

import com.seed.bank.model.Customer;

public interface CustomerService {

	public boolean saveCustomerDetails(Customer customer);

}
