package com.seed.bank.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.seed.bank.model.Account;
import com.seed.bank.repository.AccountRepository;

@Component
public class AccountServiceImpl implements AccountService {
	
	private static final Logger accountServicelogger = LoggerFactory.getLogger(AccountServiceImpl.class);

	@Autowired
	AccountRepository accountRepository;
	
	Account recepient_account;
	
	Account deductor_account;
	
	@Override
	public boolean validateAccount(Account account) {
		
		accountServicelogger.info("In AccountServiceImpl:validateAccount method");
		recepient_account = accountRepository.findByAccountNo(account.getAccountNo());
		if(recepient_account==null)
			return false;
		else
			return true;
	}

	@Override
	public boolean fundTransfer(Account account) {
		
		accountServicelogger.info("In AccountServiceImpl:fundTransfer method");
		long receipient_balance = recepient_account.getBalance();
		long deductor_balance = deductor_account.getBalance();
		long deduct_amount = account.getBalance();
		if(deductor_balance<0 && deductor_balance<0 && deduct_amount<0) {
			return false;
		}
		if(recepient_account.getAccountNo()==deductor_account.getAccountNo()) {
			return false;
		}
		receipient_balance=receipient_balance+deduct_amount;
		deductor_balance=deductor_balance-deduct_amount;
		recepient_account.setBalance(receipient_balance);
		deductor_account.setBalance(deductor_balance);
		accountRepository.save(recepient_account);
		accountRepository.save(deductor_account);
		return true;
	}

	@Override
	public boolean findByCustomerId(long customerId) {
		
		accountServicelogger.info("In AccountServiceImpl:findByCustomerId method");
		deductor_account = accountRepository.findByCustomerId(customerId);
		if(deductor_account==null) {
			return false;
		}
		return true;
	}

}
