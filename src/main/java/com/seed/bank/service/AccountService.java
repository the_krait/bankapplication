package com.seed.bank.service;

import com.seed.bank.model.Account;

public interface AccountService {
	
	public boolean validateAccount(Account account);

	public boolean fundTransfer(Account account);

	public boolean findByCustomerId(long customerId);
}
