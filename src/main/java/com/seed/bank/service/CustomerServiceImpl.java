package com.seed.bank.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.seed.bank.model.Account;
import com.seed.bank.model.Customer;
import com.seed.bank.model.User;
import com.seed.bank.repository.AccountRepository;
import com.seed.bank.repository.CustomerRepository;
import com.seed.bank.repository.UserRepository;

@Component
public class CustomerServiceImpl implements CustomerService {

	private static final Logger customerServicelogger = LoggerFactory.getLogger(CustomerServiceImpl.class);
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	UserRepository userRepository;

	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	User user;
	
	@Autowired
	Account account;
	
	@Override
	public boolean saveCustomerDetails(Customer customer) {
		
		customerServicelogger.info("In CustomerServiceImpl:saveCustomerDetails method");
		long custId=customer.getCustomerUIDAI();
		long idComb=custId+123456;
		customer.setCustomerId(idComb);
		Customer saved_customer = customerRepository.save(customer);
		if(saved_customer==null) {
			return false;
		}
		
		user.setCustomerId(idComb);
		String password = "BKI"+customer.getCustomerMobile();
		user.setCustomerPassword(password);
		User saved_user= userRepository.save(user);
		if(saved_user==null) {
			return false;
		}
		
		account.setCustomerId(idComb);
		account.setBalance(customer.getAmountDepo());
		account.setIfscCode("BIKD00530");
		account.setBranch("Pashan Road Branch, Pune");
		account.setAccountNo(customer.getCustomerUIDAI());
		account.setStatus("Active");
		Account saved_account = accountRepository.save(account);
		if(saved_account==null) {
			return false;
		}
		return true;
	}
}
