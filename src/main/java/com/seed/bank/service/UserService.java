package com.seed.bank.service;

import com.seed.bank.model.User;

public interface UserService{

	public boolean validateUser(User user);

}
