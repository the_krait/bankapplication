package com.seed.bank.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seed.bank.model.User;
import com.seed.bank.service.UserService;

@Controller
public class LoginController {

	private static final Logger loginControllerlogger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	UserService userService;
	
	@RequestMapping(value="login-page", method=RequestMethod.GET)
	public String loginPage() {
		loginControllerlogger.info("In login-page controller get method");
		return "login.jsp";
	}
	
	@RequestMapping(value="login",method=RequestMethod.POST)
	public String loginProcess(User user, ModelMap model) {
		
		loginControllerlogger.info("In login-page controller post method");
		boolean status=userService.validateUser(user);
		if(status==false) {
			model.put("errormessage", "Bad Credentials....!");
			return "login.jsp";
		}
		return "redirect:/dashboard";
	}
}
