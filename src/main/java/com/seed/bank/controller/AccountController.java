package com.seed.bank.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seed.bank.model.Account;
import com.seed.bank.service.AccountService;
import com.seed.bank.service.UserServiceImpl;

@Controller
public class AccountController {

	@Autowired
	AccountService accountService;
	
	@Autowired
	UserServiceImpl userServiceImpl;
	
	private static final Logger accountControllerlogger = LoggerFactory.getLogger(AccountController.class);
	
	@RequestMapping(value="dashboard",method=RequestMethod.GET)
	public String dashboard() {
		accountControllerlogger.info("In dashboard controller get method");
		return "dashboard.jsp";
	}
	
	@RequestMapping(value="dashboard",method=RequestMethod.POST)
	public String dashboardPage(Account account , ModelMap model, HttpSession session) {
		accountControllerlogger.info("In dashboard controller post method");
		  boolean status = accountService.validateAccount(account);
		  if(status==false) { 
			  model.put("errormessage","Bad Credentials....!"); 
			  return "dashboard.jsp"; 
		  }else {
			  boolean status1=accountService.findByCustomerId(userServiceImpl.getValidUser().getCustomerId());
			  if(status1==true) {
				  boolean status2=accountService.fundTransfer(account);
				  if(status2==true) {
					  return "redirect:/success";
				  }
				  else {
					  model.put("errormessage", "Bad Credentials...!");
					  session.invalidate();
					  return "dashboard.jsp";
				  }
			  }
			  else {
				  model.put("errormessage","Bad Credentials....!"); 
				  return "dashboard.jsp"; 
			  }
		  }
	}
	@RequestMapping(value="success",method=RequestMethod.GET)
	public String dashboardPage() {
		accountControllerlogger.info("In success controller get method");
		return "success.jsp";
	}
}
