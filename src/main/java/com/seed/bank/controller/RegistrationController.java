package com.seed.bank.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seed.bank.model.Customer;
import com.seed.bank.model.User;
import com.seed.bank.service.CustomerService;
import com.seed.bank.service.UserService;

@Controller
public class RegistrationController {
		
	private static final Logger registrationControllerlogger = LoggerFactory.getLogger(RegistrationController.class);
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	User user;
	
	@RequestMapping(value="register-page",method=RequestMethod.GET)
	public String registerPage() {
		
		registrationControllerlogger.info("In register-page get method");
		return "register.jsp";
	}
	
	@RequestMapping(value="register",method=RequestMethod.POST)
	public String registerProcess(Customer customer,ModelMap model) {
		
		registrationControllerlogger.info("In register-page post method");
		boolean status = customerService.saveCustomerDetails(customer);
		if(status==false) {
			model.put("errormessage", "Something went wrong....!");
			return "register.jsp";
		}
		return "welcome.jsp";
	}
	
	@RequestMapping(value="welcome",method=RequestMethod.GET)
	public String welcomePage() {
		
		registrationControllerlogger.info("In welcome get method");
		return "home.jsp";
	}
}