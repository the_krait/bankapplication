package com.seed.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.seed.bank.model.Account;

@Component
public interface AccountRepository extends JpaRepository<Account, Long>{

	public Account findByAccountNo(long accountNo);

	public Account findByCustomerId(long customerId);

}
