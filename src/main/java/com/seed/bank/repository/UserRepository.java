package com.seed.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.seed.bank.model.User;

@Component
public interface UserRepository extends JpaRepository<User, Long> {

	public User findByCustomerIdAndCustomerPassword(Long customerId, String custPassword);

}
