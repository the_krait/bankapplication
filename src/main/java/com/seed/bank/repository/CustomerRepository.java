package com.seed.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.seed.bank.model.Customer;

@Component
public interface CustomerRepository extends JpaRepository<Customer, Long>{

}
