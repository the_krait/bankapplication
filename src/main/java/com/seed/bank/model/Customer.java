package com.seed.bank.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Customer {
	
	@Id
	private long customerId;
	private String customerName;
	private String customerAddr;
	private long customerUIDAI;
	private String customerEmail;
	private long customerMobile;
	private long amountDepo;
	public long getCustomerId() {
		return customerId;
	}
	
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddr() {
		return customerAddr;
	}
	public void setCustomerAddr(String customerAddr) {
		this.customerAddr = customerAddr;
	}
	public long getCustomerUIDAI() {
		return customerUIDAI;
	}
	public void setCustomerUIDAI(long customerUIDAI) {
		this.customerUIDAI = customerUIDAI;
	}
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	public long getCustomerMobile() {
		return customerMobile;
	}
	public void setCustomerMobile(long customerMobile) {
		this.customerMobile = customerMobile;
	}
	public long getAmountDepo() {
		return amountDepo;
	}
	public void setAmountDepo(long amountDepo) {
		this.amountDepo = amountDepo;
	}
	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", customerName=" + customerName + ", customerAddr="
				+ customerAddr + ", customerUIDAI=" + customerUIDAI + ", customerEmail=" + customerEmail
				+ ", customerMobile=" + customerMobile + ", amountDepo=" + amountDepo + "]";
	}
}
